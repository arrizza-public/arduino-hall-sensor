// =====================================================================
// Continuously read a Hall Effect sensor and write the average to the
// serial port. Each cycle is done every 500ms. Each cycle reads the
// sensor 20 times and writes the average.
//
// Note: the hall effect sensor is not calibrated (although there are
// calibrated ones that are available for substantially higher cost).
// This means that the values reported are not absolute, but they can
// be used for relative measurements.
//
// To set Zero Offset:
//  1) temporarily change the value of ZEROOFFSET to 0
//  2) compile and upload the program to an Arduino
//  3) make sure there are no magnets or magnetic fields near the sensor
//  4) average the readings over 10 or more cycles
//  5) change the value of ZEROOFFSET to that average value
//  6) compile and upload the program to an Arduino
//  7) the readings should now be zero; if not, adjust the ZEROOFFSET
//     up or down until the readings are consistently zero.
// =====================================================================
#include "WProgram.h"

// the Hall Effect sensor pin
#define HALLSENSOR 0

// the LED is pin
#define LED 13

// Number of reads to average in every cycle
#define AVERAGEOVERNUMREADS 20

// How long to wait between reads of the sensor
#define DELAYBETWEENREADS 10

// How long to wait between cycles
#define DELAYBETWEENCYCLES 500

// the Offset for a zero reading
#define ZEROOFFSET 517

// The cycle number
int cycle = 0;

//===========================
// turn on the LED and then delay for timeout milliseconds
void ledon(int timeout)
  {
  digitalWrite(LED, HIGH);
  delay(timeout);
  }

//===========================
// turn off the LED and then delay for timeout milliseconds
void ledoff(int timeout)
  {
  digitalWrite(LED, LOW);
  delay(timeout);
  }

//===========================
// Initialize the serial port
void setup()
  {
  pinMode(LED, OUTPUT);
  Serial.begin(9600);
  cycle = 0;
  }

//===========================
// read the hall effect sensor and print the average
// over AVERAGEOVERNUMREADS loops
void loop()
  {
  cycle++;

  // turn the led on to indicate the cycle has started
  ledon(1);

  // calculate an average over many reads of the sensor
  int reading = 0;
  int i;
  for (i = 0; i < AVERAGEOVERNUMREADS; i++)
    {
    reading += analogRead(HALLSENSOR);
    delay(DELAYBETWEENREADS);
    }
  reading = reading / i;

  // adjust the reading for zero offset
  reading -= ZEROOFFSET;

  //print the reading to the serial port
  Serial.print("cycle [");
  Serial.print(cycle);
  Serial.print("]   ");
  Serial.println(reading);

  // wait for the start of the next cycle
  ledoff(DELAYBETWEENCYCLES);
  }

//===========================
int main()
  {
  init();
  setup();
  for (;;)
    {
    loop();
    }
  return 0;
  }

